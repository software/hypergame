#define BOLA_STAND 0
#define BOLA_WALK 1

#define BOLA_RIGHT 0
#define BOLA_LEFT 1

typedef struct {
	u8 df;
	u8 dc;
	u8 current;
	u8 max;
} Animate;

typedef struct {
	u8 iter;
	u8 maxIter;
	const char *sprite;
} Sprite;

typedef struct {
	u8 x;
	u8 y;
	u8 width;
	u8 height;
	u8 state;
	u8 flip;
	Animate anim;
	const char *stand[67];
	const char *walk[8];
} Bola;
				
Bola bola = {10, 10, 3, 4, BOLA_STAND, BOLA_RIGHT,
				{9, 0, 0, 67}, 
				{
					stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, 
					stand2, 
					stand3, 
					stand2, 
					stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1,
					stand4,
					stand5, stand5, stand5, stand5, stand5, stand5, stand5, stand5, stand5, stand5, stand5, stand5,
					stand4,
					stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1, stand1,
					stand6,
					stand7, stand7, stand7, stand7, stand7, stand7, stand7, stand7, stand7, stand7, stand7, stand7,
					stand6
				},
				{
					walk1,
					walk2,
					walk3,
					walk2,
					walk1,
					walk4,
					walk5,
					walk4,
				}
			};	


void AnimateBola(){
	u8 sprites_number = 0;
	if(bola.state == BOLA_STAND){
		sprites_number = sizeof(bola.stand)/2;
	}
	else if (bola.state == BOLA_WALK) {
		sprites_number = sizeof(bola.walk)/2;
	}
	
	if(++bola.anim.dc >= bola.anim.df){
		bola.anim.dc = 0;
		bola.anim.current += 1;
		
		if(bola.anim.current >= sprites_number){
			bola.anim.current = 0;
		}
	}
}

void DrawBola(){
	if(bola.state == BOLA_STAND){
		MapSprite2(0, bola.stand[bola.anim.current], bola.flip);
	}
	else if (bola.state == BOLA_WALK) {
		MapSprite2(0, bola.walk[bola.anim.current], bola.flip);
	}
	
	MoveSprite(0, bola.x, bola.y, 3, 4);
}

void ControllerBola(){
	u8 joy1 = ReadJoypad(0);
	
	u8 previousState = bola.state;
	bola.state = BOLA_STAND;
	
	if(joy1 & BTN_LEFT){
		bola.flip = BOLA_LEFT;
		bola.x -= 1;
		bola.state = BOLA_WALK;
	}
	else if(joy1 & BTN_RIGHT){
		bola.flip = BOLA_RIGHT;
		bola.x += 1;
		bola.state = BOLA_WALK;
	}
	
	if(joy1 & BTN_UP){
		bola.y -= 1;
		bola.state = BOLA_WALK;
	}
	else if(joy1 & BTN_DOWN){
		bola.y += 1;
		bola.state = BOLA_WALK;
	}
	
	if(previousState != bola.state){
		bola.anim.current = 0;
	}
}

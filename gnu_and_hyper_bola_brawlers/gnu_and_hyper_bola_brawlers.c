#include <stdbool.h>
#include <avr/io.h>
#include <stdlib.h>
#include <math.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <uzebox.h>


#include "data/tileset.inc"
#include "bola.c"

int main(){	
	ClearVram();                // Fills the video RAM with the first tile (0, 0)
	SetSpritesTileTable(tileset);
	SetTileTable(tileset);

	DrawMap2(0, 0, map);
	
	while(1){
		WaitVsync(1);
		
		ControllerBola();
		AnimateBola();
		DrawBola();
	}
}
